package framework;

import java.util.Scanner;

public class ExampleUses
{
    public static void main(String[] args)
    {
        System.out.println("Input: \"Hello\"");
        String input = new Scanner(System.in).nextLine();
        String errorString = "Error! Tough Luck! XD";
        errorString = StringTesting.stringDiff("Hello", input, errorString);
        System.out.println("errorString = " + errorString);
    }
}