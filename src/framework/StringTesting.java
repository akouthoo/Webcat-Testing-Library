package framework;

/**
 * Created by alex on 9/23/2016.
 */
public class StringTesting
{
    /**
     * Compares the output length of the expected and actual
     * Creates an error message if expected and actual don't match
     * @param errorString
     *      The errorString currently constructed. (No side effects will change this String & is nullable)
     * @param expected
     *      The expected list of output
     * @param actual
     *      The actual list of output
     * @return
     *      The new errorString with the original prepend
     */
    public static String testLineLength(String errorString, String[] expected, String[] actual)
    {
        String newErrorString = (errorString == null ? "" : errorString.toString());
        if(expected.length != actual.length)
        {
            int i;
            for (i = 0; i < actual.length; i++)
            {
                /***************************************************Don't Keep************************************************************************/
                actual[i] = actual[i].replace("\r", "");
                if(i < expected.length)
                    if(!expected[i].equalsIgnoreCase(actual[i]))
                    {
                        newErrorString += "Lines don't match starting at line " + (i + 1) + "\n" +
                        "Found \"" + actual[i] + "\" but was expecting \"" + expected[i] + "\"";
                        break;
                    }
            }
            if(i == actual.length)
                newErrorString += "Lines don't match starting at line " + (actual.length) + "\n";
        }
        return newErrorString;
    }

    /**
     * Compares the difference between the expected and actual strings.
     * Once identified will return an errorString with the param {@literal errorString} prepend to it
     * @param expected
     *      The String that the test expects
     * @param actual
     *      The String that the test actually received
     * @param errorString
     *      The errorString currently constructed. (No side effects will change this String & is nullable)
     * @return
     *      The new errorString with the original prepend
     */
    public static String stringDiff(String expected, String actual, String errorString)
    {
        // Debug
        // System.out.println(actual + "---" + expected + "---\'" + difference(actual, expected) + "\'");
        int indexDiff = indexOfDifference(actual, expected);
        if (indexDiff != -1)
        {
            String newErrorString = (errorString == null ? "" : errorString.toString());
            String diff = difference(expected, actual);
            switch (diff)
            {
                case " ":
                    newErrorString += "\nDifference: Expected \'" + ((indexDiff < expected.length()) ? expected.charAt(indexDiff) : "") + "\' but found a ' ' "+ (indexDiff == actual.length() - 1 ? "at end of line" : "at column " + indexDiff);
                    break;
                case "\'":
                    newErrorString += "\nDifference: Expected \'" + ((indexDiff < expected.length()) ? expected.charAt(indexDiff) : "") + "\' but found a [SINGLE QUOTE] "+ (indexDiff == actual.length() - 1 ? "at end of line" : "at column " + indexDiff);
                    break;
                case "\"":
                    newErrorString += "\nDifference: Expected \'" + ((indexDiff < expected.length()) ? expected.charAt(indexDiff) : "") + "\' but found a [DOUBLE QUOTE] "+ (indexDiff == actual.length() - 1 ? "at end of line" : "at column " + indexDiff);
                    break;
                case "":
                    newErrorString += "\nDifference: Expected \'" + ((indexDiff < expected.length()) ? expected.charAt(indexDiff) : "") + "\' but found [NOTHING] "+ (indexDiff == actual.length() - 1 ? "at end of line" : "at column " + indexDiff);
                    break;
                default:
                    newErrorString += "\nDifference: Expected \'" + ((indexDiff < expected.length()) ? expected.charAt(indexDiff) : "") + "\' but found \'" + diff + "\' on column " + indexDiff;
            }
            newErrorString += "\n";
            return newErrorString;
        }
        return errorString;
    }

    /**
     * Finds the first character that differs between the two given Strings
     * @param str1
     *      Comparable String 1
     * @param str2
     *      Comparable String 2
     * @return
     *      Single character String
     */
    public static String difference(String str1, String str2)
    {
        if (str1 == null)
        {
            return str2;
        }
        if (str2 == null)
        {
            return str1;
        }
        int at = indexOfDifference(str1, str2);
        if (at == -1 || str2.length() < at + 1)
        {
            return "";
        }
        return str2.substring(at, at + 1);
    }

    /**
     * Finds the index where the two given Strings differ
     * @param str1
     *      Comparable String 1
     * @param str2
     *      Comparable String 2
     * @return
     *      The index where the two Strings don't match
     */
    public static int indexOfDifference(String str1, String str2)
    {
        if (str1 == str2)
        {
            return -1;
        }
        if (str1 == null || str2 == null)
        {
            return 0;
        }
        int i;
        for (i = 0; i < str1.length() && i < str2.length(); ++i)
        {
            if (str1.charAt(i) != str2.charAt(i))
            {
                break;
            }
        }
        if (i < str2.length() || i < str1.length())
        {
            return i;
        }
        return -1;
    }
}
